#include "userform.h"
#include "ui_userform.h"

#include <QMessageBox>
#include <QValidator>
#include <QTableView>
#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>

#include "Utils.h"

UserForm::UserForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserForm)
{
    ui->setupUi(this);
    isEdited = false;
    ui->sectorCB->addItems(locals);

    ui->academicCB->addItems(academics);

    ui->sexCB->addItems(sexs);

    setList();
}

UserForm::~UserForm()
{
    delete ui;
}

void UserForm::on_savePB_clicked()
{
    if(!isEdited){
        if(isValid())
        {
            clear();
            mUser.save();
            QMessageBox::information(this, "Usuarios","Se guardo el usuario");

        }
        else
            QMessageBox::critical(this, "Usuarios","Faltan datos o ya existe el usuario" );
    }
    else{

        user.name = ui->nameLE->text();
        user.sex = ui->sexCB->currentText();
        user.age = ui->ageSB->value();
        user.place_of_origin = ui->place_of_originLE->text();
        user.expedient_number = ui->expedient_numberLE->text()+" "+codes.at(ui->sectorCB->currentIndex());
        user.sector = ui->sectorCB->currentText();
        user.DNI = ui->dniLE->text();
        user.instruction = ui->academicCB->currentText();
        user.phone = ui->phoneLE->text();
        user.isFather = ui->isFatherCB->isChecked();

        qDebug()<<user.age->toString();
        user.save();
    }
    clear();
    setList();
    ui->expedient_numberLE->setDisabled(false);
    isEdited = false;
}

bool UserForm::isValid()
{
    if(ui->nameLE->text() == "")
        return false;
    /*if(ui->place_of_originLE->text() == "")
        return false;
    if(ui->expedient_numberLE->text() == "")
        return false;
    if(ui->dniLE->text() == "")
        return false;*/

    if(User::objects()
            .filter(DQWhere("name", "=", ui->nameLE->text())
                    && DQWhere("age","=",ui->ageSB->value())
                    //&& DQWhere("place_of_origin", "=", ui->place_of_originLE->text())
                    && DQWhere("expedient_number", "=", ui->expedient_numberLE->text())
                    //&& DQWhere("sector", "=", ui->sectorCB->currentText())
                    //&& DQWhere("DNI", "=", ui->dniLE->text())
                    ).count() > 0)
        return false;
    mUser = User();
    mUser.name = ui->nameLE->text();
    mUser.sex = ui->sexCB->currentText();
    mUser.age = ui->ageSB->value();
    mUser.place_of_origin = ui->place_of_originLE->text();
    mUser.expedient_number = ui->expedient_numberLE->text()+" "+codes.at(ui->sectorCB->currentIndex());
    mUser.sector = ui->sectorCB->currentText();
    mUser.DNI = ui->dniLE->text();
    mUser.instruction = ui->academicCB->currentText();
    mUser.phone = ui->phoneLE->text();
    mUser.isFather = ui->isFatherCB->isChecked();
    return true;
}


void UserForm::clear()
{
    ui->nameLE->clear();
    ui->place_of_originLE->clear();
    ui->expedient_numberLE->clear();
    ui->expedient_numberLE->setText("-2016");
    ui->dniLE->clear();
    ui->phoneLE->clear();
    ui->isFatherCB->setCheckState(Qt::Unchecked);
}

void UserForm::setList()
{
    mUsers = User::objects().all();
    int rows = mUsers.size();
    int cols = titles.length();

    model = new QStandardItemModel(rows,cols);

    for(int col = 0; col < cols; col++)
        model->setHorizontalHeaderItem(col,new QStandardItem(titles.at(col)));


    for(int row = 0; row < rows; row++)
    {
        model->setItem(row, 0, new QStandardItem(mUsers.at(row)->name->toString()));
        model->setItem(row, 1, new QStandardItem(mUsers.at(row)->sex->toString()));
        model->setItem(row, 2, new QStandardItem(QString::number(mUsers.at(row)->age->toInt())));

        model->setItem(row, 3, new QStandardItem(mUsers.at(row)->life_project->toString()));
        model->setItem(row, 4, new QStandardItem(mUsers.at(row)->vocational_orientation->toString()));

        model->setItem(row, 5, new QStandardItem(mUsers.at(row)->place_of_origin));
        model->setItem(row, 6, new QStandardItem(mUsers.at(row)->expedient_number));
        model->setItem(row, 7, new QStandardItem(mUsers.at(row)->sector->toString()));
        model->setItem(row, 8, new QStandardItem(mUsers.at(row)->DNI->toString()));
        model->setItem(row, 9, new QStandardItem(mUsers.at(row)->phone->toString()));
        model->setItem(row, 10, new QStandardItem(mUsers.at(row)->instruction->toString()));
        model->setItem(row, 11, new QStandardItem(mUsers.at(row)->isFather->toString()));
    }
    ui->tableView->setModel(model);

}

void UserForm::updateInfo()
{
    setList();
}


void UserForm::on_tableView_doubleClicked(const QModelIndex &index)
{
    QString number =  ui->tableView->model()->index(index.row(), 6).data().toString();
    DQList<User> mSelects= User::objects().filter(DQWhere("expedient_number", "=", number)).all();
    qDebug() << mSelects.size();
    if(mSelects.size() == 1){
        User *userselected = mSelects.at(0);
        qDebug()<<userselected->expedient_number->toString();
        ui->nameLE->setText(userselected->name->toString());
        ui->dniLE->setText(userselected->DNI->toString());
        ui->expedient_numberLE->setText(userselected->expedient_number->toString());
        ui->expedient_numberLE->setDisabled(true);

        ui->phoneLE->setText(userselected->phone->toString());
        ui->place_of_originLE->setText(userselected->place_of_origin->toString() );

        ui->academicCB->setCurrentText(userselected->instruction->toString());

        ui->sexCB->setCurrentText(userselected->sex->toString());

        ui->sectorCB->setCurrentText(userselected->sector->toString());

        ui->ageSB->setValue(userselected->age->toInt());

        ui->isFatherCB->setChecked(userselected->isFather->toBool());
        user.load(DQWhere("id") == userselected->id);
        isEdited = true;
    }
}
