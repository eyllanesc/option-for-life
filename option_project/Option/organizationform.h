#ifndef ORGANIZATIONFORM_H
#define ORGANIZATIONFORM_H

#include <QWidget>

#include <QStandardItemModel>

#include "models/models.h"

#include "Utils.h"

namespace Ui {
class OrganizationForm;
}

/**
 * @brief
 *
 */
class OrganizationForm : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit OrganizationForm(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~OrganizationForm();

    void setId(int id);

private slots:
    /*!
     \brief

     \fn on_addPB_clicked
    */
    void on_addPB_clicked();

private:
    Ui::OrganizationForm *ui; /**< TODO: describe */

    Organization organization;

    int mId; /*!< TODO: describe */

    DQList<Organization> mOrganizations; /*!< TODO: describe */

    /*!
     \brief

     \fn setList
    */
    void setList();
    QStandardItemModel *model; /*!< TODO: describe */
};

#endif // ORGANIZATIONFORM_H
