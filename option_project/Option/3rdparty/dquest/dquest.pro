INCLUDEPATH += src/$$PWD

QT += sql

TEMPLATE = lib
TARGET = dquest
CONFIG += staticlib

INCLUDEPATH += $$PWD/include

QMAKE_CXXFLAGS += -Wno-invalid-offsetof

HEADERS += \
    include/dqabstractmodel.h \
    include/dqbasefield.h \
    include/dqclause.h \
    include/dqconnection.h \
    include/dqexpression.h \
    include/dqfield.h \
    include/dqforeignkey.h \
    include/dqindex.h \
    include/dqlist.h \
    include/dqlistwriter.h \
    include/dqmetainfoquery_p.h \
    include/dqmodel.h \
    include/dqmodelmetainfo.h \
    include/dqquery.h \
    include/dqqueryrules.h \
    include/dqsharedlist.h \
    include/dqsharedquery_p.h \
    include/dqsharedquery.h \
    include/dqsql.h \
    include/dqsqlitestatement.h \
    include/dqsqlstatement.h \
    include/dqstream.h \
    include/dquest.h \
    include/dqwhere_p.h \
    include/dqwhere.h

SOURCES += \
    src/dqabstractmodel.cpp \
    src/dqbasefield.cpp \
    src/dqclause.cpp \
    src/dqconnection.cpp \
    src/dqexpression.cpp \
    src/dqfield.cpp \
    src/dqindex.cpp \
    src/dqlistwriter.cpp \
    src/dqmodel.cpp \
    src/dqmodelmetainfo.cpp \
    src/dqqueryrules.cpp \
    src/dqsharedlist.cpp \
    src/dqsharedquery.cpp \
    src/dqsql.cpp \
    src/dqsqlitestatement.cpp \
    src/dqsqlstatement.cpp \
    src/dqstream.cpp \
    src/dqwhere.cpp

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DESTDIR = $$PWD/lib
