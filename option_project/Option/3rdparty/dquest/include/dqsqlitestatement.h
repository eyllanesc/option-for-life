#ifndef DQSQLITESTATEMENT_H
#define DQSQLITESTATEMENT_H

#include <QVariant>
#include <dqsqlstatement.h>

/// Sqlite SQL Statement generator

/**
    @remarks It is thread-safe
    @remarks All the derived class should not hold any member attribute.
 */
class DQSqliteStatement : public DQSqlStatement
{
public:
    /**
     * @brief
     *
     */
    DQSqliteStatement();

    /**
     * @brief
     *
     * @param type
     * @return QString
     */
    QString columnTypeName(QVariant::Type type);
    /**
     * @brief
     *
     * @param clause
     * @return QString
     */
    QString columnConstraint(DQClause clause);

    /**
     * @brief
     *
     * @return QString
     */
    virtual QString driverName();

    /// Check is a table exist
    /**
     * @brief
     *
     * @param info
     * @return QString
     */
    static QString exists(DQModelMetaInfo *info);

protected:

    /**
     * @brief
     *
     * @param info
     * @return QString
     */
    virtual QString _createTableIfNotExists(DQModelMetaInfo *info);

private:

};

#endif // DQSQLITESTATEMENT_H
