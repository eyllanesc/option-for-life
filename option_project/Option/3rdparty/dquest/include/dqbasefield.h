#ifndef DQBASEFIELD_H
#define DQBASEFIELD_H

#include <QSharedDataPointer>
#include <QVariant>
#include <dqclause.h>

class DQModel;

/// The base class of DQField

/**
 * @brief
 *
 */
class DQBaseField
{
public:
    /**
     * @brief
     *
     */
    DQBaseField();
    /**
     * @brief
     *
     */
    ~DQBaseField();

    /// Assign value to the field
    /**
     * @brief
     *
     * @param value
     * @return bool
     */
    virtual bool set(QVariant value);

    /// Get the value of the field
    /**
      @param convert True if the QVariant return should be converted to a type which is suitable for saving.

      Some data type like QStringList is not suitable for saving. User may override
      this function and convert its to other type.
     */
    virtual QVariant get(bool convert = false) const;

    /// The default clause of that field type
    /**
     * @brief
     *
     * @return DQClause
     */
    static DQClause clause();

    /// Assign the value from a QVariant type source.
    /**
     * @brief
     *
     * @param val
     * @return QVariant operator
     */
    virtual QVariant operator=(const QVariant &val);

    /// Provides access to stored QVariant value
    /**
     * @brief
     *
     * @return QVariant *operator ->
     */
    QVariant* operator->();

    /// Get the value of the field
    /**
     * @brief
     *
     * @param )(
     * @return QVariant operator
     */
    QVariant operator() ()const;

    /// Cast to QVariant
    /**
     * @brief
     *
     * @return operator
     */
    operator QVariant();

    /// Free up any resources used.
    /**
     * @brief
     *
     */
    void clear();

private:
    QVariant m_value; /**< TODO: describe */
};

/**
 * @brief
 *
 * @param dbg
 * @param field
 * @return QDebug operator
 */
QDebug operator<<(QDebug dbg, const DQBaseField &field);


#endif // DQBASEFIELD_H
