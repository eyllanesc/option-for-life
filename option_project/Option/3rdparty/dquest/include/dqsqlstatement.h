#ifndef DQSQLSTATEMENT_H
#define DQSQLSTATEMENT_H

#include <QString>

#include <dqmodelmetainfo.h>
#include <dqsharedquery.h>
#include <dqqueryrules.h>
#include <dqindex.h>

class DQSharedQuery;
class DQQueryRules;

/// Sql Statement generator abstract interface

/**
 * @brief
 *
 */
class DQSqlStatement
{
public:
    /// Default constructor
    /**
     * @brief
     *
     */
    DQSqlStatement();

    /// Get the supported driver name
    /**
     * @brief
     *
     * @return QString
     */
    virtual QString driverName() = 0;

    /// "CREATE TABLE IF NOT EXISTS" statement
    template <typename T>
    /**
     * @brief
     *
     * @return QString
     */
    QString createTableIfNotExists() {
        DQModelMetaInfo *info = dqMetaInfo<T>();
        return _createTableIfNotExists(info);
    }

    /// "CREATE TABLE IF NOT EXISTS" statement
    /**
     * @brief
     *
     * @param info
     * @return QString
     */
    virtual QString createTableIfNotExists(DQModelMetaInfo *info);

    /// "DROP TABLE" statement
    template <typename T>
    /**
     * @brief
     *
     * @return QString
     */
    QString dropTable() {
        DQModelMetaInfo *info = dqMetaInfo<T>();
        return dropTable(info);
    }

    /// Drop table statement
    /**
     * @brief
     *
     * @param info
     * @return QString
     */
    virtual QString dropTable(DQModelMetaInfo *info);

    /// Create index statement
    /**
     * @brief
     *
     * @param index
     * @return QString
     */
    virtual QString createIndexIfNotExists(const DQBaseIndex& index);

    /// Drop the index
    /**
     * @brief
     *
     * @param name
     * @return QString
     */
    virtual QString dropIndexIfExists(QString name);

    /// Insert into statement
    /**
      @param with_id TRUE if the "id" field should be included.
     */
    virtual QString insertInto(DQModelMetaInfo *info,QStringList fields);

    /// Replace into statement
    /**
      @param with_id TRUE if the "id" field should be included.
     */
    virtual QString replaceInto(DQModelMetaInfo *info,QStringList fields);

    /// Select statement
    /**
     * @brief
     *
     * @param query
     * @return QString
     */
    virtual QString select(DQSharedQuery query);

    /// Delete from statement
    /**
     * @brief
     *
     * @param query
     * @return QString
     */
    virtual QString deleteFrom(DQSharedQuery query);

    /// Returns a string representation of the QVariant for SQL statement
    /**
     * @brief
     *
     * @param value
     * @param trimStrings
     * @return QString
     */
    virtual QString formatValue(QVariant value,bool trimStrings = false);

protected:
    /// The real function for create table if not exists
    /**
     * @brief
     *
     * @param info
     * @return QString
     */
    virtual QString _createTableIfNotExists(DQModelMetaInfo *info) = 0;

    /// The real function for "insert into / replace into" statement
    /**
     * @brief
     *
     * @param info
     * @param type
     * @param fields
     * @return QString
     */
    virtual QString _insertInto(DQModelMetaInfo *info ,QString type, QStringList fields);

    /**
     * @brief
     *
     * @param rules
     * @return QString
     */
    virtual QString selectCore(DQQueryRules rules);

    /**
     * @brief
     *
     * @param rules
     * @return QString
     */
    virtual QString selectResultColumn(DQQueryRules rules);

    /**
     * @brief
     *
     * @param limit
     * @param offset
     * @return QString
     */
    virtual QString limitAndOffset(int limit, int offset = 0);

    /**
     * @brief
     *
     * @param rules
     * @return QString
     */
    virtual QString orderBy(DQQueryRules rules);

};


#endif // DQSQLSTATEMENT_H
