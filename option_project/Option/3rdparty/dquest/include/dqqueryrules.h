#ifndef DQQUERYRULES_H
#define DQQUERYRULES_H

#include <QSharedDataPointer>
#include <dqsharedquery.h>
#include <dqexpression.h>

/// DQQueryRules represent the rules/clauses set for DQSharedQuery.

/** DQSharedQuery/DQQuery do not provide any interface to get the rules / clause set for query.
  Instead it should use DQQueryRules to retrieve the information. Normally user do not need to
  retreive the rules set for a query, it is useful for implement custom select sql or unit tests
 */

class DQQueryRules
{
public:
    /**
     * @brief
     *
     */
    DQQueryRules();
    /**
     * @brief
     *
     * @param
     */
    DQQueryRules(const DQQueryRules &);
    /**
     * @brief
     *
     * @param
     * @return DQQueryRules &operator
     */
    DQQueryRules &operator=(const DQQueryRules &);
    /**
     * @brief
     *
     * @param
     * @return DQQueryRules &operator
     */
    DQQueryRules &operator=(const DQSharedQuery &);
    /**
     * @brief
     *
     */
    ~DQQueryRules();

    /// Get the limit of query
    /**
     * @brief
     *
     * @return int
     */
    int limit();

    /**
     * @brief
     *
     * @return DQExpression
     */
    DQExpression expression();

    /// Get the func that should be applied on result column
    /**
     * @brief
     *
     * @return QString
     */
    QString func();

    /// Get the DQModelMetaInfo instance of the query model
    /**
     * @brief
     *
     * @return DQModelMetaInfo
     */
    DQModelMetaInfo *metaInfo();

    /// Get the field for result column
    /**
     * @brief
     *
     * @return QStringList
     */
    QStringList fields();

    /// Get the field for orderBy
    /**
     * @brief
     *
     * @return QStringList
     */
    QStringList orderBy();

private:
    QSharedDataPointer<DQSharedQueryPriv> data; /**< TODO: describe */
};

#endif // DQQUERYRULES_H
