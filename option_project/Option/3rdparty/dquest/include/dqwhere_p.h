#ifndef DQWHERE_P_H
#define DQWHERE_P_H

#include <QVariant>

/// A private database structure used by DQWhere & DQExpression
/** It is used to store the data for special operator.
 */
class DQWhereDataPriv {

public:

    /**
     * @brief
     *
     */
    enum Type {
        None,
        In,
        Between
    };

    /**
     * @brief
     *
     */
    DQWhereDataPriv();

    /**
     * @brief
     *
     * @param type
     */
    DQWhereDataPriv(Type type);

    /**
     * @brief
     *
     * @param v
     * @return DQWhereDataPriv &operator
     */
    DQWhereDataPriv& operator<<(QVariant v);

    /**
     * @brief
     *
     * @return QList<QVariant>
     */
    QList<QVariant> list();

    /**
     * @brief
     *
     * @param list
     */
    void setList(QList<QVariant> list);

    /**
     * @brief
     *
     * @return Type
     */
    Type type();

private:
    QList<QVariant> m_list; /**< TODO: describe */
    Type m_type; /**< TODO: describe */
};

Q_DECLARE_METATYPE(DQWhereDataPriv)


#endif // DQWHERE_P_H
