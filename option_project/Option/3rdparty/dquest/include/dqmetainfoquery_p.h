#ifndef DQMETAINFOQUERY_P_H
#define DQMETAINFOQUERY_P_H

#include "dqsharedquery.h"

/// Query class for internal use

/**
 * @brief
 *
 */
class _DQMetaInfoQuery : public DQSharedQuery {
public:
    /**
     * @brief
     *
     * @param metaInfo
     * @param connection
     */
    inline _DQMetaInfoQuery(DQModelMetaInfo *metaInfo,DQConnection connection) : DQSharedQuery(connection) , m_metaInfo(metaInfo){
        setMetaInfo(metaInfo);
    }

    /**
     * @brief
     *
     * @param rhs
     * @return _DQMetaInfoQuery &operator
     */
    _DQMetaInfoQuery& operator=(const DQSharedQuery &rhs ) {
        DQSharedQuery::operator =(rhs);
        return *this;
    }

    /**
     * @brief
     *
     * @param model
     * @return bool
     */
    bool recordTo(DQModel *model) {
        return DQSharedQuery::recordTo(model);
    }

    DQModelMetaInfo *m_metaInfo; /**< TODO: describe */
};

#endif // DQMETAINFOQUERY_P_H
