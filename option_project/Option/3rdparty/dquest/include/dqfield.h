#ifndef DQFIELD_H
#define DQFIELD_H

#include <dqbasefield.h>

/// Database field
/**
    DQField store the value of a field in database model. The format is QVariant.
    Therefore you may assign a QVariant to DQField direclty, and you may access
    the stored QVariant by using operator-> or get() function.

    @see DQModel
    @see DQForeignKey
 */

template <typename T>
class DQField : public DQBaseField
{
public:
    /// Default constructor
    /**
     * @brief
     *
     */
    DQField(){
    }

    /// Return the type id of the field
    /**
     * @brief
     *
     * @return QVariant::Type
     */
    static QVariant::Type type(){
        return (QVariant::Type) qMetaTypeId<T>();
    }

    /// Copy the value from a QVariant object
    /**
     * @brief
     *
     * @param val
     * @return QVariant operator
     */
    inline QVariant operator=(const QVariant &val){
        set(val);
        return val;
    }

    /// Compare with other DQField
    /**
     * @brief
     *
     * @param rhs
     * @return bool operator
     */
    inline bool operator==(const DQField& rhs) const {
        return get() == rhs.get();
    }

    /// Compare with QVariant type
    /**
     * @brief
     *
     * @param rhs
     * @return bool operator
     */
    inline bool operator==(const QVariant &rhs) const {
        return get() == rhs;
    }

    /// Compare with QVariant type
    /**
     * @brief
     *
     * @param rhs
     * @return bool operator
     */
    inline bool operator!=(const QVariant &rhs) const {
        return get() != rhs;
    }

    /// Compare with its template type
    /**
     * @brief
     *
     * @param t
     * @return bool operator
     */
    inline bool operator==(const T& t) const {
        return get() == t;
    }

    /// Compare with its template type
    /**
     * @brief
     *
     * @param t
     * @return bool operator
     */
    inline bool operator!=(const T& t) const {
        return get() != t;
    }

    /// Compare with string type
    /**
     * @brief
     *
     * @param string
     * @return bool operator
     */
    inline bool operator==(const char *string) const {
        return get() == QString(string);
    }

    /// Compare with string type
    /**
     * @brief
     *
     * @param string
     * @return bool operator
     */
    inline bool operator!=(const char *string) const {
        return get() != QString(string);
    }

    /// Get the value of the field
    /**
     * @brief
     *
     * @param convert
     * @return QVariant
     */
    inline QVariant get(bool convert = false) const {
        return DQBaseField::get(convert);
    }

    /// Set the value of the field
    /**
     * @brief
     *
     * @param value
     * @return bool
     */
    inline bool set(QVariant value) {
        return DQBaseField::set(value);
    }

    /// Cast it to the template type
    /**
     * @brief
     *
     * @return operator
     */
    inline operator T() const {
        QVariant v = get();
        return v.value<T>();
    }

};

template <>
/**
 * @brief
 *
 * @param value
 * @return bool DQField<QStringList>
 */
bool DQField<QStringList>::set(QVariant value);

template <>
/**
 * @brief
 *
 * @param convert
 * @return QVariant DQField<QStringList>
 */
QVariant DQField<QStringList>::get(bool convert) const;

/// Primary key field

/**
 * @brief
 *
 */
class DQPrimaryKey : public DQField<int> {
public:
    /**
     * @brief
     *
     */
    DQPrimaryKey();
    /**
     * @brief
     *
     * @return DQClause
     */
    static DQClause clause();

    /**
     * @brief
     *
     * @param val
     * @return QVariant operator
     */
    inline QVariant operator=(const QVariant &val){
        set(val);
        return val;
    }
};

#endif // DQFIELD_H
