#ifndef DQEXPRESSION_H
#define DQEXPRESSION_H

#include <dqwhere.h>
#include <QMap>
#include <QSharedDataPointer>

class DQExpressionPriv;

/// Construct an expression based on DQWhere clause
/**
   @remarks It is a private class for implementation use. User should not use this class.
 */
class DQExpression
{
public:
    /**
     * @brief
     *
     */
    DQExpression();
    /**
     * @brief
     *
     * @param rhs
     */
    DQExpression(const DQExpression& rhs);
    /**
     * @brief
     *
     * @param where
     */
    DQExpression(DQWhere where);
    /**
     * @brief
     *
     * @param rhs
     * @return DQExpression &operator
     */
    DQExpression &operator=(const DQExpression &rhs);

    /**
     * @brief
     *
     */
    ~DQExpression();

    /// Get the expression in string
    /**
     * @brief
     *
     * @return QString
     */
    QString string();

    /// A map of values to find with QSqlQuery
    /**
     * @brief
     *
     * @return QMap<QString, QVariant>
     */
    QMap<QString,QVariant> bindValues();

    /**
     * @brief
     *
     * @return bool
     */
    bool isNull();

private:

    QSharedDataPointer<DQExpressionPriv> d; /**< TODO: describe */
};

#endif // DQEXPRESSION_H
