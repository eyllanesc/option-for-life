#ifndef DQSQL_H
#define DQSQL_H

#include <QExplicitlySharedDataPointer>
#include <QSqlQuery>
#include <dqmodelmetainfo.h>
#include <dqindex.h>

class DQModelMetaInfo;
class DQSqlStatement;
class DQModel;

class DQSqlStatement;

class DQSqlPriv;

/// A helper class for SQL statement exeuction
/**
   DQSql provides a set of pre-defined SQL operation over the connected database.
   It should be a thread safe object (not verified).

   User are not supposed to use this class except for error checking. User
   may call lastQuery() to retreive the detailed information of last
   query. It is useful to debug SQL level error.

   @remarks The thread safe capability is not verified.
 */

class DQSql
{
public:
    /// Copy constructor
    /**
     * @brief
     *
     * @param
     */
    DQSql(const DQSql&);

    /// Default constructor
    /**
     * @brief
     *
     */
    ~DQSql();

    /// Get the associated DQSqlStatement generator
    /**
     * @brief
     *
     * @return DQSqlStatement
     */
    DQSqlStatement* statement();

    /// The connected database
    /**
     * @brief
     *
     * @return QSqlDatabase
     */
    QSqlDatabase database();

    /// Run create table of a model
    template <typename T>
    /**
     * @brief
     *
     * @return bool
     */
    inline bool createTableIfNotExists(){
        DQModelMetaInfo* metaInfo = dqMetaInfo<T>();
        return createTableIfNotExists(metaInfo);
    }

    /// Run create table of a model
    /**
     * @brief
     *
     * @param info
     * @return bool
     */
    bool createTableIfNotExists(DQModelMetaInfo* info);

    /// Run drop table of a model
    /**
     * @brief
     *
     * @param info
     * @return bool
     */
    bool dropTable(DQModelMetaInfo* info);

    /// Create index
    /**
     * @brief
     *
     * @param index
     * @return bool
     */
    bool createIndexIfNotExists(const DQBaseIndex &index);

    /// Drop index
    /**
     * @brief
     *
     * @param name
     * @return bool
     */
    bool dropIndexIfExists(QString name);

    /// Is the model exists on database?
    /**
     * @brief
     *
     * @param info
     * @return bool
     */
    bool exists(DQModelMetaInfo* info);

    /// Insert the reocrd to the database.
    /**
      @param info The meta information of writing model
      @param model The data source
      @param fields A list of fields that should be saved
      @param updateId TRUE if the ID of the model should be updated after operation
     */
    bool insertInto(DQModelMetaInfo* info,DQModel *model,QStringList fields,bool updateId);

    /// Replace the record to the database
    /**
      @param info The meta information of writing model
      @param model The data source
      @param fields A list of fields that should be saved
      @param updateId TRUE if the ID of the model should be updated after operation
     */
    bool replaceInto(DQModelMetaInfo* info,DQModel *model,QStringList fields,bool updateId);

    /// Create a query object to the connected database
    /**
     * @brief
     *
     * @return QSqlQuery
     */
    QSqlQuery query();

    /// The last query object
    /**
     * @brief
     *
     * @return QSqlQuery
     */
    QSqlQuery lastQuery();

protected:
    /**
      @param statement A instance of DQSqlStatement. The ownership will be taken.
     */

    explicit DQSql(DQSqlStatement *statement = 0);

    /// Assignment operator overloading
    /**
     * @brief
     *
     * @param rhs
     * @return DQSql &operator
     */
    DQSql& operator=(const DQSql &rhs);

    /// Set the connected database
    /**
     * @brief
     *
     * @param db
     */
    void setDatabase(QSqlDatabase db);

    /// Assign a SQL statement generator
    /**
      @param statement A instance of DQSqlStatement. The ownership will be taken.
     */
    void setStatement(DQSqlStatement *statement);

private:
    /**
     * @brief
     *
     * @param query
     */
    void setLastQuery(QSqlQuery query);

    /**
     * @brief
     *
     * @param info
     * @param model
     * @param fields
     * @param with_id
     * @param replace
     * @return bool
     */
    bool insertInto(DQModelMetaInfo* info,DQModel *model,QStringList fields,bool with_id,bool replace);

    QExplicitlySharedDataPointer<DQSqlPriv> d; /**< TODO: describe */

    friend class DQConnection;
    friend class DQConnectionPriv;
};

#endif // DQSQL_H
