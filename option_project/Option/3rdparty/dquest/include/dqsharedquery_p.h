#ifndef DQABSTRACTQUERY_P_H
#define DQABSTRACTQUERY_P_H

#include <QSqlQuery>
#include "dqconnection.h"
#include "dqmodel.h"
#include "dqmodelmetainfo.h"
#include "dqwhere.h"
#include "dqexpression.h"

/// DQSharedQuery private data

/**
 * @brief
 *
 */
class DQSharedQueryPriv : public QSharedData {
public:
    /**
     * @brief
     *
     */
    inline DQSharedQueryPriv() {
        metaInfo = 0;
        limit = -1; // No limit
    }

    DQConnection connection; /**< TODO: describe */

    /// The function to be called on result column.
    QString func; /**< TODO: describe */

    DQModelMetaInfo *metaInfo; /**< TODO: describe */
    int limit; /**< TODO: describe */

    QSqlQuery query; /**< TODO: describe */

    DQExpression expression; /**< TODO: describe */

    /// select(fields)
    QStringList fields; /**< TODO: describe */

    QStringList orderBy; /**< TODO: describe */
};

#endif // DQABSTRACTQUERY_P_H
