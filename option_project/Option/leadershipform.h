#ifndef LEADERSHIPFORM_H
#define LEADERSHIPFORM_H

#include <QWidget>

#include "models/models.h"

namespace Ui {
class LeadershipForm;
}

/**
 * @brief
 *
 */
class LeadershipForm : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit LeadershipForm(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~LeadershipForm();

    void updateInfo();

    /*!
     \brief

     \fn setId
     \param id
    */
    void setId(int id);

private slots:
    /**
     * @brief
     *
     */
    void on_savePB_clicked();

    void on_numberLE_textChanged(const QString &number);

    /*!
     \brief

     \fn on_localCB_currentIndexChanged
     \param index
    */
    void on_localCB_currentIndexChanged(int index);

    /*!
     \brief

     \fn on_toDE_dateChanged
     \param date
    */
    void on_toDE_dateChanged(const QDate &date);

    void on_dateEdit_2_dateChanged(const QDate &date);

private:
    Ui::LeadershipForm *ui; /**< TODO: describe */
    /**
     * @brief
     *
     * @param q
     */
    void filterByUser(const QString &q="", int index=0);
    /**
     * @brief
     *
     */
    void init();

    Leadership leadership; /**< TODO: describe */
    DQList<User> mUsers; /**< TODO: describe */

    QStringList titles_leadership; /**< TODO: describe */
    /**
     * @brief
     *
     * @param q
     */
    void filterByDate(const QDate &from, const QDate &to);
    QStringList titles; /**< TODO: describe */

    bool isValid();

    int mId; /*!< TODO: describe */
};

#endif // LEADERSHIPFORM_H
