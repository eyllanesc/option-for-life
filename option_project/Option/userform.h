#ifndef USERFORM_H
#define USERFORM_H

#include <QWidget>

#include <QStandardItemModel>
#include <dqlist.h>

#include "models/user.h"

namespace Ui {
class UserForm;
}

/**
 * @brief
 *
 */
class UserForm : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit UserForm(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~UserForm();

    void updateInfo();

private slots:
    /**
     * @brief
     *
     */
    void on_savePB_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::UserForm *ui; /**< TODO: describe */
    User mUser; /**< TODO: describe */
    DQList<User> mUsers; /**< TODO: describe */

    /**
     * @brief
     *
     * @return bool
     */
    bool isValid();
    /**
     * @brief
     *
     */
    void clear();
    /**
     * @brief
     *
     */
    void setList();

    QStandardItemModel *model; /**< TODO: describe */

    bool isEdited;

    User user;

};

#endif // USERFORM_H
