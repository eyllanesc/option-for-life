#ifndef RESULTSFORM_H
#define RESULTSFORM_H

#include <QWidget>
#include <QtCharts>

#include <QBarSet>
#include <QStackedBarSeries>

#include "models/models.h"

using namespace QtCharts;

namespace Ui {
class ResultsForm;
}

/**
 * @brief
 *
 */
class ResultsForm : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit ResultsForm(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~ResultsForm();

    void updateInfo();

private slots:
    /**
     * @brief
     *
     * @param index
     */
    void on_optionCB_currentIndexChanged(int index);

    void on_fromDE_dateChanged(const QDate &date);

    /**
     * @brief
     *
     * @param date
     */
    void on_toDE_dateChanged(const QDate &date);

private:
    Ui::ResultsForm *ui; /**< TODO: describe */

    /**
     * @brief
     *
     */
    void init();
    /**
     * @brief
     *
     */
    enum{
        all_index,
        psychological_index,
        leadership_index,
        fretime_index
    };

    /**
     * @brief
     *
     * @param from
     * @param to
     */
    void calculatePsychological( const QDate &from, const QDate &to);
    /**
     * @brief
     *
     * @param from
     * @param to
     */
    void calculateLeadership( const QDate &from, const QDate &to);
    /**
     * @brief
     *
     * @param from
     * @param to
     */
    void calculateFreetime( const QDate &from, const QDate &to);


    /**
     * @brief
     *
     * @param index
     * @param from
     * @param to
     */
    void process(int index, const QDate &from, const QDate &to);
    /**
     * @brief
     *
     * @param age
     * @return int
     */
    int getCategory(const int& age);
    /**
     * @brief
     *
     */
    enum{
        children,
        adolescent,
        young,
        adult
    };

    /**
     * @brief
     *
     * @param id
     * @param age
     * @param sex
     */
    void calculate(int id, int age, QString sex);

    QList<int> male_list; /**< TODO: describe */
    QList<int> female_list; /**< TODO: describe */

    QList<int> children_male_list; /**< TODO: describe */
    QList<int> children_female_list;
    QList<int> adolescent_male_list; /**< TODO: describe */
    QList<int> adolescent_female_list;
    QList<int> young_male_list; /**< TODO: describe */
    QList<int> young_female_list;
    QList<int> adult_male_list; /**< TODO: describe */
    QList<int> adult_female_list;

    int total; /**< TODO: describe */
    int tmp_rows; /**< TODO: describe */
    int age; /**< TODO: describe */
    QString sex; /**< TODO: describe */
    int id; /**< TODO: describe */

    QBarSet *s_male;
    QBarSet *s_female;

    QStackedBarSeries *series; /**< TODO: describe */

    QChart *chart; /**< TODO: describe */

    QValueAxis *axisY; /**< TODO: describe */
};

#endif // RESULTSFORM_H
