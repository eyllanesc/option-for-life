#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>

#include <simplecrypt.h>

#include "models/models.h"

namespace Ui {
class AuthDialog;
}

/*!
 \brief

 \class AuthDialog authdialog.h "authdialog.h"
*/
class AuthDialog : public QDialog
{
    Q_OBJECT

public:
    /*!
     \brief

     \fn AuthDialog
     \param parent
    */
    explicit AuthDialog(QWidget *parent = 0);
    /*!
     \brief

     \fn ~AuthDialog
    */
    ~AuthDialog();
    /*!
     \brief

     \fn isValid
     \return bool
    */
    bool isValid();

    /*!
     \brief

     \fn createuser
    */
    void createuser();
    /*!
     \brief

     \fn getId
     \return int
    */
    int getId();
private:
    Ui::AuthDialog *ui; /*!< TODO: describe */

    int id; /*!< TODO: describe */

    Auth auth; /*!< TODO: describe */
    SimpleCrypt crypto; /*!< TODO: describe */
};

#endif // AUTHDIALOG_H
