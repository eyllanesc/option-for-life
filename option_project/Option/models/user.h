#ifndef USER_H
#define USER_H

#include <dquest.h>
#include <QDebug>

/**
 * @brief
 *
 */
class User : public DQModel {
    DQ_MODEL
    public:

    DQField<QString> name; /**< TODO: describe */
    DQField<QString> sex; /**< TODO: describe */
    DQField<uint> age; /**< TODO: describe */
    DQField<bool> vocational_orientation; /**< TODO: describe */
    DQField<bool> life_project; /**< TODO: describe */
    DQField<QString> place_of_origin; /**< TODO: describe */
    DQField<QString> expedient_number; /**< TODO: describe */
    DQField<QString> sector; /**< TODO: describe */
    DQField<QDateTime> create_date; /**< TODO: describe */
    DQField<QDateTime> last_updated; /**< TODO: describe */
    DQField<QString> DNI;
    DQField<QString> phone;
    DQField<QString> instruction;
    DQField<bool> isFather; /*!< TODO: describe */

    /**
     * @brief
     *
     * @return bool
     */
    bool clean(){
        last_updated = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
        return true;
    }
};

DQ_DECLARE_MODEL(User,
                 "users", // the table name.
                 DQ_FIELD(name),
                 DQ_FIELD(sex),
                 DQ_FIELD(age),
                 DQ_FIELD(vocational_orientation,  DQDefault(false)),
                 DQ_FIELD(life_project,  DQDefault(false)),
                 DQ_FIELD(place_of_origin),
                 DQ_FIELD(expedient_number),
                 DQ_FIELD(sector),
                 DQ_FIELD(create_date , DQDefault("CURRENT_TIMESTAMP") ),
                 DQ_FIELD(last_updated),
                 DQ_FIELD(DNI),
                 DQ_FIELD(phone),
                 DQ_FIELD(instruction),
                 DQ_FIELD(isFather, DQDefault(false))
                 )

#endif // USER_H
