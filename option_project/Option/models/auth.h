#ifndef AUTH_H
#define AUTH_H

#include <dquest.h>

/*!
 \brief

 \class Auth auth.h "auth.h"
*/
class Auth : public DQModel {
    DQ_MODEL
    public:
    DQField<QString> username; /**< TODO: describe */
    DQField<QString> password;
};

DQ_DECLARE_MODEL(Auth,
                 "auths", // the table name.
                 DQ_FIELD(username),
                 DQ_FIELD(password)
                 )

#endif // AUTH_H
