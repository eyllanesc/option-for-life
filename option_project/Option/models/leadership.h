#ifndef LEADERSHIP_H
#define LEADERSHIP_H

#include <dquest.h>


/**
 * @brief
 *
 */
class Leadership : public DQModel {
    DQ_MODEL
    public:
    DQField<QString> theme; /**< TODO: describe */
    DQField<QDate> date; /**< TODO: describe */
    DQField<QString> local;
};

DQ_DECLARE_MODEL(Leadership,
                 "leaderships", // the table name.
                 DQ_FIELD(theme),
                 DQ_FIELD(date),
                 DQ_FIELD(local)
                 )

#endif // LEADERSHIP_H
