#ifndef ASSISTANCE_LEADERSHIP_H
#define ASSISTANCE_LEADERSHIP_H

#include <dquest.h>

#include "user.h"
#include "leadership.h"
#include "auth.h"

/**
 * @brief
 *
 */
class Assistance_Leadership : public DQModel {
    DQ_MODEL
    public:
    DQForeignKey <User> user; /**< TODO: describe */
    DQForeignKey <Leadership> leadership; /**< TODO: describe */
    DQForeignKey <Auth> auth;
};

DQ_DECLARE_MODEL(Assistance_Leadership,
                 "assistance_leaderships", // the table name.
                 DQ_FIELD(user),
                 DQ_FIELD(leadership),
                 DQ_FIELD(auth)
                 )

#endif // ASSISTANCE_LEADERSHIP_H
