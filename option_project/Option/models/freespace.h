#ifndef FREESPACE_H
#define FREESPACE_H

#include <dquest.h>
#include "auth.h"

/*!
 \brief

 \class Freespace freespace.h "freespace.h"
*/
class Freespace : public DQModel {
    DQ_MODEL
    public:
    DQForeignKey <Auth> auth; /*!< TODO: describe */
    DQField<QString> local; /*!< TODO: describe */
    DQField<QString> activity; /*!< TODO: describe */
    DQField<QDate> fromDate; /*!< TODO: describe */
    DQField<QDate> toDate; /*!< TODO: describe */
};

DQ_DECLARE_MODEL(Freespace,
                 "freespaces", // the table name.
                 DQ_FIELD(auth),
                 DQ_FIELD(local),
                 DQ_FIELD(activity),
                 DQ_FIELD(fromDate),
                 DQ_FIELD(toDate)
                 )

#endif // FREESPACE_H
