#ifndef ORGANIZATION_H
#define ORGANIZATION_H

#include <dquest.h>
#include "auth.h"

/*!
 \brief

 \class Organization organization.h "organization.h"
*/
class Organization : public DQModel {
    DQ_MODEL
    public:
    DQField<QString> local; /*!< TODO: describe */
    DQField<QString> name; /*!< TODO: describe */
    DQField<QString> site; /*!< TODO: describe */
    DQField<QString> description; /*!< TODO: describe */
    DQField<uint> number_participants; /*!< TODO: describe */
    DQForeignKey<Auth> auth; /*!< TODO: describe */
};

DQ_DECLARE_MODEL(Organization,
                 "organizations", // the table name.
                 DQ_FIELD(local),
                 DQ_FIELD(name),
                 DQ_FIELD(site),
                 DQ_FIELD(description),
                 DQ_FIELD(number_participants),
                 DQ_FIELD(auth)
                 )

#endif // ORGANIZATION_H
