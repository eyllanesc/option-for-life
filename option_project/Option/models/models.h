#ifndef MODELS_H
#define MODELS_H

#include "user.h"

#include "psychological.h"
#include "assistance_psychological.h"

#include "leadership.h"
#include "assistance_leadership.h"

#include "freetime.h"
#include "assistance_freetime.h"

#include "auth.h"

#include "organization.h"

#include "freespace.h"

#endif // MODELS_H
