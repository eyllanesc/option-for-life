#ifndef FREETIME_H
#define FREETIME_H

#include <dquest.h>

/**
 * @brief
 *
 */
class Freetime : public DQModel {
    DQ_MODEL
    public:
    DQField<QString> activity; /**< TODO: describe */
    DQField<QString> place;
    DQField<QDate> date; /**< TODO: describe */
    DQField<QString> local;
};

DQ_DECLARE_MODEL(Freetime,
                 "freetimes", // the table name.
                 DQ_FIELD(activity),
                 DQ_FIELD(place),
                 DQ_FIELD(date),
                 DQ_FIELD(local)
                 )

#endif // FREETIME_H
