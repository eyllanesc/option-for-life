#ifndef ASSISTANCE_FREETIME_H
#define ASSISTANCE_FREETIME_H

#include <dquest.h>
#include "user.h"
#include "freetime.h"
#include "auth.h"

/**
 * @brief
 *
 */
class Assistance_Freetime : public DQModel {
    DQ_MODEL
    public:
    DQForeignKey <User> user; /**< TODO: describe */
    DQForeignKey <Freetime> freetime; /**< TODO: describe */
    DQForeignKey <Auth> auth;
};

DQ_DECLARE_MODEL(Assistance_Freetime,
                 "assistance_freetimes", // the table name.
                 DQ_FIELD(user),
                 DQ_FIELD(freetime),
                 DQ_FIELD(auth)
                 )

#endif // ASSISTANCE_FREETIME_H
