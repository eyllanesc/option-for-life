#ifndef PSYCHOLOGICAL_H
#define PSYCHOLOGICAL_H

#include <dquest.h>

/**
 * @brief
 *
 */
class Psychological : public DQModel {
    DQ_MODEL
    public:
    DQField<QString> local; /**< TODO: describe */
    DQField<QDate> date; /**< TODO: describe */
};

DQ_DECLARE_MODEL(Psychological,
                 "psychologicals", // the table name.
                 DQ_FIELD(local),
                 DQ_FIELD(date)
                 )

#endif // PSYCHOLOGICAL_H
