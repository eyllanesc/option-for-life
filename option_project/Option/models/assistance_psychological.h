#ifndef ASSISTANCE_PSYCHOLOGICAL_H
#define ASSISTANCE_PSYCHOLOGICAL_H

#include <dquest.h>

#include "user.h"
#include "psychological.h"
#include "auth.h"

/**
 * @brief
 *
 */
class Assistance_Psychological : public DQModel {
    DQ_MODEL
public:
    DQForeignKey<User> user; /**< TODO: describe */
    DQForeignKey<Psychological> psychological; /**< TODO: describe */
    DQField<QString> reason; /**< TODO: describe */
    DQForeignKey<Auth> auth;
};

DQ_DECLARE_MODEL(Assistance_Psychological,
                 "assistance_psychologicals", // the table name.
                 DQ_FIELD(user),
                 DQ_FIELD(psychological),
                 DQ_FIELD(reason),
                 DQ_FIELD(auth)
                 )

#endif // ASSISTANCE_PSYCHOLOGICAL_H
