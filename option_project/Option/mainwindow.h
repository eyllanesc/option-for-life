#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <dqconnection.h>

#include "models/models.h"

#include "userform.h"
#include "psychologicalform.h"
#include "leadershipform.h"
#include "freetimeform.h"
#include "resultsform.h"
#include "organizationform.h"
#include "freespaceform.h"

#include "authdialog.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief
 *
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~MainWindow();

private:
    Ui::MainWindow *ui; /**< TODO: describe */
    /**
     * @brief
     *
     */
    void initDB();
    QSqlDatabase db; /**< TODO: describe */
    DQConnection connection; /**< TODO: describe */

    UserForm *userform; /**< TODO: describe */
    PsychologicalForm *psychologicalform; /**< TODO: describe */
    LeadershipForm *leadershipform;
    FreetimeForm *freetimeform;
    ResultsForm *resultsform;
    OrganizationForm *organizationform; /**< TODO: describe */
    FreespaceForm *freespaceform; /**< TODO: describe */

    AuthDialog *authdialog;

    /*!
     \brief

     \fn validateAuth
     \param status
    */
    void validateAuth(int status);
    /**
     * @brief
     *
     */
    enum{
        user_index,
        psychological_index,
        leadership_index,
        freetime_index,
        result_index
    };
private slots:
    /**
     * @brief
     *
     * @param index
     */
    void currentChangedSlot(int index);
    void aboutQt();
    void about();
protected:
    /**
     * @brief
     *
     * @param event
     */
    void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;
};

#endif // MAINWINDOW_H
