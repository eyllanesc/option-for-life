#ifndef FREETIMEFORM_H
#define FREETIMEFORM_H

#include <QWidget>

#include "models/models.h"

namespace Ui {
class FreetimeForm;
}

/**
 * @brief
 *
 */
class FreetimeForm : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit FreetimeForm(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~FreetimeForm();

    void updateInfo();

    /*!
     \brief

     \fn setId
     \param id
    */
    void setId(int id);

private slots:
    /**
     * @brief
     *
     */
    void on_savePB_clicked();

    void on_numberLE_textChanged(const QString &number);

    /*!
     \brief

     \fn on_localCB_currentIndexChanged
     \param index
    */
    void on_localCB_currentIndexChanged(int index);

    /*!
     \brief

     \fn on_toDE_dateChanged
     \param date
    */
    void on_toDE_dateChanged(const QDate &date);

    void on_dateEdit_2_dateChanged(const QDate &date);

private:
    Ui::FreetimeForm *ui; /**< TODO: describe */
    /**
     * @brief
     *
     */
    void init();

    bool isSaveFreetime; /**< TODO: describe */

    /**
     * @brief
     *
     * @param q
     */
    void filterByUser(const QString &q="", int index=0);
    /**
     * @brief
     *
     * @param q
     */
    void filterByDate(const QDate &from, const QDate &to);

    DQList<User> mUsers; /**< TODO: describe */

    Freetime freetime;

    /**
     * @brief
     *
     * @return bool
     */
    bool isValid();

    int mId;
};

#endif // FREETIMEFORM_H
