#ifndef FREESPACEFORM_H
#define FREESPACEFORM_H

#include <QWidget>

#include "models/models.h"

#include <QStandardItemModel>

namespace Ui {
class FreespaceForm;
}

/**
 * @brief
 *
 */
class FreespaceForm : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit FreespaceForm(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~FreespaceForm();

    void setId(int id);

private slots:

    /*!
     \brief

     \fn on_addPB_clicked
    */
    void on_addPB_clicked();

private:
    Ui::FreespaceForm *ui; /**< TODO: describe */

    Freespace freespace;
    DQList<Freespace> mFreespaces;

    /*!
     \brief

     \fn setList
    */
    void setList();
    QStandardItemModel *model; /*!< TODO: describe */
    int mId; /*!< TODO: describe */
};

#endif // FREESPACEFORM_H
