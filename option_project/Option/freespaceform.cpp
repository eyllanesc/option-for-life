#include "freespaceform.h"
#include "ui_freespaceform.h"

#include "Utils.h"

#include <QMessageBox>

#include <QDebug>
#include <QCalendarWidget>
#include <QLocale>

FreespaceForm::FreespaceForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FreespaceForm)
{
    ui->setupUi(this);
    ui->localCB->addItems(locals);
    setList();

    ui->fromDE->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));
    ui->toDE->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));

    ui->fromDE->setDate(QDate::currentDate());
    ui->toDE->setDate(QDate::currentDate());

}

FreespaceForm::~FreespaceForm()
{
    delete ui;
}

void FreespaceForm::setList()
{

    mFreespaces = Freespace::objects().all();
    int rows = mFreespaces.size();
    int cols = titles_freespace.length();

    model = new QStandardItemModel(rows,cols);

    for(int col = 0; col < cols; col++)
        model->setHorizontalHeaderItem(col,new QStandardItem(titles_freespace.at(col)));


    for(int row = 0; row < rows; row++)
    {
        model->setItem(row, 0, new QStandardItem(mFreespaces.at(row)->local->toString()));
        model->setItem(row, 1, new QStandardItem(mFreespaces.at(row)->activity->toString()));
        model->setItem(row, 2, new QStandardItem(mFreespaces.at(row)->fromDate->toString()));
        model->setItem(row, 3, new QStandardItem(mFreespaces.at(row)->toDate->toString()));
    }
    ui->tableView->setModel(model);
}

void FreespaceForm::on_addPB_clicked()
{    
    freespace = Freespace();
    freespace.auth = mId;
    freespace.local = ui->localCB->currentText();
    freespace.activity = ui->activityLE->text();
    freespace.fromDate = ui->fromDE->date().toString("yyyy-MM-dd");
    freespace.toDate = ui->toDE->date().toString("yyyy-MM-dd");

    qDebug()<<ui->fromDE->date().toString("yyyy-MM-dd");

    freespace.save();

    QMessageBox::information(this, "Usuarios","Se guardo la organización");
    setList();
}

void FreespaceForm::setId(int id)
{
    mId = id;
}
