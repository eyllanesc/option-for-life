#include "organizationform.h"
#include "ui_organizationform.h"

#include "Utils.h"

#include <QMessageBox>

OrganizationForm::OrganizationForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OrganizationForm)
{
    ui->setupUi(this);
    ui->localCB->addItems(locals);
    setList();
}

OrganizationForm::~OrganizationForm()
{
    delete ui;
}

void OrganizationForm::on_addPB_clicked()
{
    organization = Organization();
    organization.local = ui->localCB->currentText();
    organization.name = ui->nameLE->text();
    organization.site = ui->siteLE->text();
    organization.description = ui->descriptionLE->text();
    organization.number_participants = ui->numberSB->value();
    organization.auth = mId;
    organization.save();
    QMessageBox::information(this, "Usuarios","Se guardo la organización");
    setList();
}

void OrganizationForm::setList()
{
        mOrganizations = Organization::objects().all();
        int rows = mOrganizations.size();
        int cols = titles_organization.length();

        model = new QStandardItemModel(rows,cols);

        for(int col = 0; col < cols; col++)
            model->setHorizontalHeaderItem(col,new QStandardItem(titles_organization.at(col)));


        for(int row = 0; row < rows; row++)
        {
            model->setItem(row, 0, new QStandardItem(mOrganizations.at(row)->name->toString()));
            model->setItem(row, 1, new QStandardItem(mOrganizations.at(row)->local->toString()));
            model->setItem(row, 2, new QStandardItem(mOrganizations.at(row)->site->toString()));
            model->setItem(row, 3, new QStandardItem(mOrganizations.at(row)->description->toString()));
            model->setItem(row, 4, new QStandardItem(QString::number(mOrganizations.at(row)->number_participants->toInt())));
        }
        ui->tableView->setModel(model);
}

void OrganizationForm::setId(int id)
{
    mId = id;
}
