#include "mainwindow.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setApplicationName("Opcion por la Vida");
    app.setApplicationDisplayName("Opción por la Vida");
    app.setOrganizationName("CodeHuntersLab");
    app.setOrganizationDomain("codehunterslab.com");
    app.setApplicationVersion("1.0");

    app.setStyle("fusion");

    MainWindow w;
    w.showMaximized();

    return app.exec();
}
