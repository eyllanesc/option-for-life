#include "authdialog.h"
#include "ui_authdialog.h"

#include <dquest.h>
#include <QDebug>
#include "Utils.h"

AuthDialog::AuthDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDialog)
{
    ui->setupUi(this);

    crypto= SimpleCrypt(Q_UINT64_C(0x0c2ad4a4acb9f023));

    ui->usernameLE->setText("Nadia");
    ui->passwordLE->setText("47602910");

    if(Auth::objects().count() == 0)
        createuser();
}

AuthDialog::~AuthDialog()
{
    delete ui;
}


bool AuthDialog::isValid()
{

    DQList<Auth> list = Auth::objects().filter(DQWhere("username", "=", ui->usernameLE->text())).all();

    int tam = list.size();

    for(int i=0; i < tam; i++){
        if(ui->passwordLE->text() == crypto.decryptToString(list.at(i)->password)){
            id = list.at(i)->id;
            return true;
        }
    }
    return false;
}

void AuthDialog::createuser()
{
    auth = Auth();
    auth.username = "Alejandra";
    auth.password = crypto.encryptToString(QString("47564795"));
    auth.save();

    auth = Auth();
    auth.username = "Giangabriel";
    auth.password = crypto.encryptToString(QString("72783026"));
    auth.save();

    auth = Auth();
    auth.username = "Adriana";
    auth.password = crypto.encryptToString(QString("73128341"));
    auth.save();

    auth = Auth();
    auth.username = "Gianmarco";
    auth.password = crypto.encryptToString(QString("72497378"));
    auth.save();

    auth = Auth();
    auth.username = "Susy";
    auth.password = crypto.encryptToString(QString("72302726"));
    auth.save();

    auth = Auth();
    auth.username = "Nadia";
    auth.password = crypto.encryptToString(QString("47602910"));
    auth.save();

}

int AuthDialog::getId()
{
   return id;
}
