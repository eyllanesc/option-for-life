#include "psychologicalform.h"
#include "ui_psychologicalform.h"
#include <QCompleter>
#include <QLocale>
#include <QCalendarWidget>
#include <QDateEdit>
#include <QStandardItem>

#include <QMessageBox>

#include <QDebug>

#include "Utils.h"

PsychologicalForm::PsychologicalForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PsychologicalForm)
{
    ui->setupUi(this);
    init();
}

void PsychologicalForm::init()
{
    ui->themeCB->addItems(themes);

    ui->localCB->addItems(locals);

    ui->dateEdit->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));
    ui->dateEdit->setDate(QDate::currentDate());

    ui->dateEdit_2->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));
    ui->dateEdit_2->setDate(QDate::currentDate());

    ui->toDE->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));
    ui->toDE->setDate(QDate::currentDate());
    filterByUser();
}

void PsychologicalForm::setId(int id)
{
    mId = id;
}

PsychologicalForm::~PsychologicalForm()
{
    delete ui;
}

void PsychologicalForm::updateInfo()
{
    filterByUser();
}


void PsychologicalForm::on_toDE_dateChanged(const QDate &date)
{
    filterByDate(ui->dateEdit_2->date() , date);
}

void PsychologicalForm::filterByDate(const QDate &from, const QDate &to)
{

    DQList<Psychological> psychologicals = Psychological::objects().filter(DQWhere("date",">=", from.toString("yyyy-MM-dd"))
                                                                           && DQWhere("date","<=", to.toString("yyyy-MM-dd"))).all();

    int length = psychologicals.size();

    DQList<Assistance_Psychological> assistances;

    DQList<Assistance_Psychological> tmp;

    int tmp_rows;
    for(int i=0; i < length; i++){
        tmp = Assistance_Psychological::objects().filter(DQWhere("psychological")==psychologicals.at(i)->id).all();
        tmp_rows = tmp.size();

        for(int row= 0; row < tmp_rows; row++){
            assistances<<*tmp.at(row);
        }
    }


    int rows = assistances.size();
    int cols = titles_psychological.length();

    QStandardItemModel *model = new QStandardItemModel(rows,cols);

    for(int col = 0; col < cols; col++){
        model->setHorizontalHeaderItem(col,new QStandardItem(titles_psychological.at(col)));
    }

    for(int row = 0; row < rows; row++)
    {
        model->setItem(row,0, new QStandardItem(assistances.at(row)->user->name));
        model->setItem(row,1, new QStandardItem(assistances.at(row)->reason));
        model->setItem(row,2, new QStandardItem(assistances.at(row)->psychological->local));

    }
    ui->listTV->setModel(model);
}


void PsychologicalForm::on_savePB_clicked()
{
    if(Psychological::objects()
            .filter(DQWhere("date", "=", ui->dateEdit->date().toString("yyyy-MM-dd"))&&
                    DQWhere("local", "=", ui->localCB->currentText()))
            .count() == 0)
    {
        psychological = Psychological();
        psychological.date = ui->dateEdit->date().toString("yyyy-MM-dd");
        psychological.local = ui->localCB->currentText();
        psychological.save();
        qDebug()<<"saving psychological";
    }
    else
    {
        psychological.load(DQWhere("date", "=", ui->dateEdit->date().toString("yyyy-MM-dd")) &&
                           DQWhere("local" , "=", ui->localCB->currentText()) );
    }

    int nUsers = mUsers.size();
    if((nUsers == 0) || (ui->numberLE->text()==""))
    {
        QMessageBox::critical(this, "Usuarios", "Seleccione al menos un usuario");
        return;
    }
    for(int i=0; i < nUsers; i++){
        if(Assistance_Psychological::objects()
                .filter(DQWhere("psychological", "=", psychological.id)&&
                        DQWhere("user", "=",mUsers.at(i)->id)&&
                        DQWhere("reason", "=", ui->themeCB->currentText()))
                .count()==0)
        {
            Assistance_Psychological assistance;
            assistance.user = mUsers.at(i)->id;
            assistance.psychological = psychological.id;
            assistance.reason = ui->themeCB->currentText();
            assistance.auth = mId;
            QMessageBox::information(this, "Usuarios","Se guardo la asistencia de "+ mUsers.at(i)->name);
            assistance.save();
        }
    }

    filterByDate(ui->dateEdit_2->date(), ui->dateEdit->date());
}


void PsychologicalForm::filterByUser(const QString &q, int index)
{
    if(q=="")
        mUsers = User::objects().all();
    else
        mUsers = User::objects()
                .filter(DQWhere("expedient_number")== q+" "+codes.at(index)).all();

    int rows = mUsers.size();
    int cols = titles.length();

    QStandardItemModel *model = new QStandardItemModel(rows,cols);

    for(int col = 0; col < cols; col++)
        model->setHorizontalHeaderItem(col,new QStandardItem(titles.at(col)));

    for(int row = 0; row < rows; row++)
    {
        model->setItem(row,0, new QStandardItem(mUsers.at(row)->name));
        model->setItem(row,1, new QStandardItem(mUsers.at(row)->sex));
        model->setItem(row,2, new QStandardItem(QString::number(mUsers.at(row)->age->toInt())));

        model->setItem(row,3, new QStandardItem(mUsers.at(row)->life_project->toString()));
        model->setItem(row,4, new QStandardItem(mUsers.at(row)->vocational_orientation->toString()));

        model->setItem(row,5, new QStandardItem(mUsers.at(row)->place_of_origin));
        model->setItem(row,6, new QStandardItem(mUsers.at(row)->expedient_number));
        model->setItem(row,7, new QStandardItem(mUsers.at(row)->sector));
    }
    ui->filterTV->setModel(model);
}

void PsychologicalForm::on_numberLE_textChanged(const QString &number)
{
    filterByUser(number, ui->localCB->currentIndex());
}

void PsychologicalForm::on_localCB_currentIndexChanged(int index)
{
    filterByUser( ui->numberLE->text(), index);
}

void PsychologicalForm::on_dateEdit_2_dateChanged(const QDate &date)
{
    filterByDate(date , ui->dateEdit->date());
}
