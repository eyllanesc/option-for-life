#ifndef PSYCHOLOGICALFORM_H
#define PSYCHOLOGICALFORM_H

#include <QWidget>

#include <QStandardItemModel>

#include "models/assistance_psychological.h"
#include "models/psychological.h"

namespace Ui {
class PsychologicalForm;
}

/**
 * @brief
 *
 */
class PsychologicalForm : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit PsychologicalForm(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~PsychologicalForm();

    void updateInfo();


    /*!
     \brief

     \fn setId
     \param id
    */
    void setId(int id);
private slots:

    /**
     * @brief
     *
     * @param date
     */
    void on_toDE_dateChanged(const QDate &date);
    /**
     * @brief
     *
     */
    void on_savePB_clicked();

    void on_numberLE_textChanged(const QString &number);

    /*!
     \brief

     \fn on_localCB_currentIndexChanged
     \param index
    */
    void on_localCB_currentIndexChanged(int index);

    void on_dateEdit_2_dateChanged(const QDate &date);

private:
    Ui::PsychologicalForm *ui; /**< TODO: describe */

    DQList<User> mUsers; /**< TODO: describe */
    DQList<Psychological> psychologicals; /**< TODO: describe */


    /**
     * @brief
     *
     * @param q
     */
    void filterByUser(const QString &q="", int index=0);
    /**
     * @brief
     *
     * @param q
     */
    void filterByDate(const QDate &from, const QDate &to);

    Psychological psychological; /**< TODO: describe */

    void init();

    int mId; /*!< TODO: describe */

};

#endif // PSYCHOLOGICALFORM_H
