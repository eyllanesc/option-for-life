#-------------------------------------------------
#
# Project created by QtCreator 2016-10-17T23:48:01
#
#-------------------------------------------------

QT       += core gui sql charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OPV
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    userform.cpp \
    psychologicalform.cpp \
    leadershipform.cpp \
    freetimeform.cpp \
    resultsform.cpp \
    organizationform.cpp \
    freespaceform.cpp \
    authdialog.cpp

HEADERS  += mainwindow.h \
    models/user.h \
    models/psychological.h \
    models/assistance_psychological.h \
    models/leadership.h \
    models/assistance_leadership.h \
    models/models.h \
    models/freetime.h \
    models/assistance_freetime.h \
    userform.h \
    psychologicalform.h \
    leadershipform.h \
    freetimeform.h \
    resultsform.h \
    organizationform.h \
    freespaceform.h \
    Utils.h \
    models/auth.h \
    authdialog.h \
    models/organization.h \
    models/freespace.h

FORMS    += mainwindow.ui \
    psychologicalform.ui \
    userform.ui \
    leadershipform.ui \
    freetimeform.ui \
    resultsform.ui \
    organizationform.ui \
    freespaceform.ui \
    authdialog.ui

LIBS += -L$$PWD/3rdparty/dquest/lib/ -ldquest
INCLUDEPATH += $$PWD/3rdparty/dquest/include

LIBS += -L$$PWD/3rdparty/SimpleCrypt/lib/ -lSimpleCrypt
INCLUDEPATH += $$PWD/3rdparty/SimpleCrypt/include

QMAKE_CXXFLAGS += -Wno-invalid-offsetof

RESOURCES += \
    resource.qrc

OTHER_FILES += option.ico \
    LICENSE

DESTDIR = $$PWD/../output

win32: RC_ICONS = option.ico

