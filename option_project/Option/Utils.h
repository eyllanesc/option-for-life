#ifndef UTILS_H
#define UTILS_H

#include <QStringList>

#include <dquest.h>


static const QStringList locals =  QStringList() << "Torreblanca" /*!< TODO: describe */
                                                 << "La Flor"
                                                 <<"San Pedro"
                                                <<"Lomas"
                                               <<"Raul Porras"
                                              <<"30 de Mayo";

static const QStringList titles  =  QStringList() << "Nombres" /*!< TODO: describe */
                                                  << "Sexo"
                                                  << "Edad"
                                                  << "Orientación Vocacional"
                                                  <<"Proyecto de Vida"
                                                 <<"Lugar de Procedencia"
                                                <<"Expediente"
                                               <<"sector"
                                              <<"DNI"
                                             <<"teléfono"
                                            <<"Grade de Intrucción"
                                           <<"Padre de Familia";

static const QStringList academics = QStringList() << "Primaria Completa" /*!< TODO: describe */
                                                   << "Primaria Incompleta"
                                                   << "Secundaria Completa"
                                                   << "Secundaria Incompleta"
                                                   << "Técnico Superior Completo"
                                                   << "Técnico Superior Incompleto"
                                                   << "Universitario Superior Completo"
                                                   << "Universitario Superior Incompleto";

static const QStringList categories = QStringList()<< "Niños" /*!< TODO: describe */
                                                   << "Adolescentes"
                                                   << "Jovenes"
                                                   <<"Adultos";

static const QStringList titles_freetime = QStringList()<< "Usuario" /*!< TODO: describe */
                                                        << "Actividad"
                                                        << "Espacio público recuperado"
                                                        <<"Local";

static const QStringList titles_psychological = QStringList() << "Usuario" /*!< TODO: describe */
                                                              <<"Tema"
                                                             <<"Local";

static const QStringList titles_option = QStringList() <<  "Todas" /*!< TODO: describe */
                                                        << "Asistencias Psicológicas"
                                                        << "Liderazgo"
                                                        << "Tiempo Libre";

static const QStringList themes = QStringList() <<"Problemas de conducta" <<"Enamoramiento"<<"Orientación vocacional" /*!< TODO: describe */
                                               <<"Hábitos de estudio"<<"Depresión "<<"Bulling"
                                              <<"Autoestima"<<"Miedo a hablar en público"<<"Deserción escolar"
                                             <<"Consumo de sustancias"<<"Adicciones a las redes sociales"<<"Adiccion al juego"
                                            <<"Trastorno de conducta alimentaria";

static const QStringList sexs= QStringList() << "Masculino" /*!< TODO: describe */
                                             << "Femenino";

static const QStringList codes = QStringList() << "TB" /*!< TODO: describe */
                                               << "LF"
                                               << "SP"
                                               << "LM"
                                               << "RPB"
                                               << "30M";

static const QStringList titles_organization  =  QStringList() << "Nombre" /*!< TODO: describe */
                                                               << "Sector"
                                                               << "Descripción"
                                                               << "Lugar"
                                                               << "Número de Participantes";

static const QStringList titles_freespace  =  QStringList() << "Local" /*!< TODO: describe */
                                                               << "Desde"
                                                               << "Hasta"
                                                               << "Lugar";

#endif // UTILS_H
