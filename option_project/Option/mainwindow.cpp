#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

#include <QMessageBox>

#include <QDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initDB();

    authdialog = new AuthDialog(this);

    userform= new UserForm(this);
    ui->tabWidget->addTab(userform, "Usuarios");

    psychologicalform = new PsychologicalForm(this);
    ui->tabWidget->addTab(psychologicalform, "Atenciones Psicológicas");

    leadershipform = new LeadershipForm(this);
    ui->tabWidget->addTab(leadershipform, "Liderazgo");

    organizationform = new OrganizationForm(this);
    ui->tabWidget->addTab(organizationform, "Organizaciones Juveniles");

    freetimeform = new FreetimeForm(this);
    ui->tabWidget->addTab(freetimeform, "Tiempo Libre");

    freespaceform = new FreespaceForm(this);
    ui->tabWidget->addTab(freespaceform, "Espacio Público Recuperados");

    resultsform = new ResultsForm(this);
    ui->tabWidget->addTab(resultsform, "Resultados");


    connect(ui->tabWidget , SIGNAL(currentChanged(int)),this,SLOT(currentChangedSlot(int)));
    connect(ui->aboutQt, SIGNAL(triggered()), this, SLOT(aboutQt()));
    connect(ui->about, SIGNAL(triggered()), this, SLOT(about()));

    bool closedialog = false;

    while(!closedialog)
    {
        if(authdialog->exec() == QDialog::Accepted)
        {
            if(authdialog->isValid())
                closedialog = true;
        }
        else{
            QTimer::singleShot(0, this, SLOT(close()));
            break;
        }
    }

    int id = authdialog->getId();

    psychologicalform->setId(id);
    leadershipform->setId(id);
    freetimeform->setId(id);
    organizationform->setId(id);
    freespaceform->setId(id);

}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initDB()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName( "option.db" );
    db.open();
    connection.open(db);
    connection.addModel<User>();

    connection.addModel<Psychological>();
    connection.addModel<Assistance_Psychological>();

    connection.addModel<Leadership>();
    connection.addModel<Assistance_Leadership>();

    connection.addModel<Freetime>();
    connection.addModel<Assistance_Freetime>();

    connection.addModel<Auth>();

    connection.addModel<Organization>();

    connection.addModel<Freespace>();

    connection.createTables();
}


void MainWindow::currentChangedSlot(int index)
{
    switch (index) {
    case user_index:
        userform->updateInfo();
        break;
    case psychological_index:
        psychologicalform->updateInfo();
        break;
    case leadership_index:
        leadershipform->updateInfo();
        break;
    case freetime_index:
        freetimeform->updateInfo();
        break;
    case result_index:
        resultsform->updateInfo();
        break;
    default:
        break;
    }
}

void MainWindow::aboutQt()
{
    QMessageBox::aboutQt(this, "Acerca de Qt");
}

void MainWindow::about()
{
    QMessageBox::about(this, "Option 1.0", "Sofware desarrollado para calcular "
                                           "las estadísticas de el Programa Opción por la Vida"
                                           "<br>Código fuente: "
                                           "<a href= \"https://bitbucket.org/eyllanesc/option-for-life\">https://bitbucket.org/eyllanesc/option-for-life</a>"
                                           "<br>Autor: Edwin Yllanes <a href=\"mailto:e.yllanescucho@gmail.com\">e.yllanescucho@gmail.com</a>"
                                           "<br>Licencia LGPL");
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    connection.close();
    qDebug()<<"close";
    QMainWindow::closeEvent(event);
}
