#include "freetimeform.h"
#include "ui_freetimeform.h"

#include <QCalendarWidget>
#include <QLocale>
#include <QStandardItemModel>
#include <QDateEdit>
#include <QLineEdit>

#include <QMessageBox>
#include "Utils.h"

FreetimeForm::FreetimeForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FreetimeForm)
{
    ui->setupUi(this);
    init();
}

FreetimeForm::~FreetimeForm()
{
    delete ui;
}

void FreetimeForm::updateInfo()
{
    filterByUser();
}

void FreetimeForm::init()
{

    ui->localCB->addItems(locals);

    ui->dateEdit->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));

    ui->toDE->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));

    ui->dateEdit->setDate(QDate::currentDate());
    ui->toDE->setDate(QDate::currentDate());

    ui->dateEdit_2->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));
    ui->dateEdit_2->setDate(QDate::currentDate());

    filterByUser();
    isSaveFreetime = false;
}

void FreetimeForm::filterByUser(const QString &q, int index)
{
    if(q=="")
        mUsers = User::objects().all();
    else
        mUsers = User::objects().filter(DQWhere("expedient_number") ==q + " " + codes.at(index)).all();

    int rows = mUsers.size();
    int cols = titles.length();

    QStandardItemModel *model = new QStandardItemModel(rows,cols);

    for(int col = 0; col < cols; col++)
        model->setHorizontalHeaderItem(col,new QStandardItem(titles.at(col)));

    for(int row = 0; row < rows; row++)
    {
        model->setItem(row,0, new QStandardItem(mUsers.at(row)->name));
        model->setItem(row,1, new QStandardItem(mUsers.at(row)->sex));
        model->setItem(row,2, new QStandardItem(QString::number(mUsers.at(row)->age->toInt())));

        model->setItem(row,3, new QStandardItem(mUsers.at(row)->life_project->toBool()));
        model->setItem(row,4, new QStandardItem(mUsers.at(row)->vocational_orientation->toBool()));

        model->setItem(row,5, new QStandardItem(mUsers.at(row)->place_of_origin));
        model->setItem(row,6, new QStandardItem(mUsers.at(row)->expedient_number));
        model->setItem(row,7, new QStandardItem(mUsers.at(row)->sector));
    }
    ui->filterTV->setModel(model);
}

void FreetimeForm::filterByDate(const QDate &from, const QDate &to)
{
    DQList<Freetime> freetimes = Freetime::objects().filter(DQWhere("date",">=", from.toString("yyyy-MM-dd"))
                                                            && DQWhere("date","<=", to.toString("yyyy-MM-dd"))).all();

    int length = freetimes.size();

    DQList<Assistance_Freetime> assistances;

    DQList<Assistance_Freetime> tmp;

    int tmp_rows;
    for(int i=0; i < length; i++){
        tmp = Assistance_Freetime::objects().filter(DQWhere("freetime")==freetimes.at(i)->id).all();
        tmp_rows = tmp.size();

        for(int row= 0; row < tmp_rows; row++){
            assistances<<*tmp.at(row);
        }
    }


    int rows = assistances.size();
    int cols = titles_freetime.length();

    QStandardItemModel *model = new QStandardItemModel(rows,cols);

    for(int col = 0; col < cols; col++){
        model->setHorizontalHeaderItem(col,new QStandardItem(titles_freetime.at(col)));
    }

    for(int row = 0; row < rows; row++)
    {
        model->setItem(row,0, new QStandardItem(assistances.at(row)->user->name));
        model->setItem(row,1, new QStandardItem(assistances.at(row)->freetime->activity));
        model->setItem(row,2, new QStandardItem(assistances.at(row)->freetime->place));
        model->setItem(row,3, new QStandardItem(assistances.at(row)->freetime->local));
    }
    ui->listTV->setModel(model);
}

bool FreetimeForm::isValid()
{
    if(ui->activityLE->text() == "")
        return false;
    if(ui->placeLE->text() == "")
        return false;
    freetime = Freetime();
    freetime.date = ui->dateEdit->date().toString("yyyy-MM-dd");
    freetime.local = ui->localCB->currentText();
    freetime.activity = ui->activityLE->text();
    freetime.place = ui->placeLE->text();
    return true;
}

void FreetimeForm::setId(int id)
{
    mId = id;
}



void FreetimeForm::on_toDE_dateChanged(const QDate &date)
{
    filterByDate(ui->dateEdit_2->date(), date);
}

void FreetimeForm::on_savePB_clicked()
{
    if(Freetime ::objects()
            .filter(DQWhere("date", "=", ui->dateEdit->date().toString("yyyy-MM-dd"))&&
                    DQWhere("local", "=", ui->localCB->currentText())&&
                    DQWhere("place", "=", ui->placeLE->text())&&
                    DQWhere("activity", "=", ui->activityLE->text()))
            .count() == 0)
    {
        if(isValid())
            freetime.save();
        else
        {
            QMessageBox::critical(this, "Tiempo Libre", "Existen campos vacios");
            return;
        }
    }
    else{
        freetime.load(DQWhere("date", "=", ui->dateEdit->date().toString("yyyy-MM-dd"))&&
                      DQWhere("local" , "=", ui->localCB->currentText())&&
                      DQWhere("place" , "=",ui->placeLE->text())&&
                      DQWhere("activity" , "=",ui->activityLE->text()));
    }

    int nUsers = mUsers.size();
    if((nUsers == 0) || (ui->numberLE->text()=="")){
        QMessageBox::critical(this, "Usuarios", "Seleccione al menos un usuario");
        return;
    }


    for(int i=0; i < nUsers; i++)
    {
        if(Assistance_Freetime::objects()
                .filter(DQWhere("freetime", "=", freetime.id) &&
                        DQWhere("user", "=", mUsers.at(i)->id))
                .count()==0)
        {
            Assistance_Freetime assistance;
            assistance.user = mUsers.at(i)->id;
            assistance.freetime = freetime.id;
            assistance.auth = mId;
            QMessageBox::information(this, "Usuarios","Se guardo la asistencia de "+ mUsers.at(i)->name);
            assistance.save();
        }
    }
    filterByDate(ui->dateEdit_2->date(), ui->dateEdit->date());
}

void FreetimeForm::on_numberLE_textChanged(const QString &number)
{
    filterByUser(number, ui->localCB->currentIndex());
}

void FreetimeForm::on_localCB_currentIndexChanged(int index)
{
    filterByUser(ui->numberLE->text(), index);
}

void FreetimeForm::on_dateEdit_2_dateChanged(const QDate &date)
{
    filterByDate(ui->dateEdit_2->date(), date);
}
