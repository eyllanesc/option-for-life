#include "resultsform.h"
#include "ui_resultsform.h"

#include <QCalendarWidget>
#include <QLocale>
#include <QDebug>
#include <QColor>

#include "Utils.h"

ResultsForm::ResultsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResultsForm)
{
    ui->setupUi(this);
    s_male = new QBarSet("Masculino");
    s_male->setColor(QColor(0, 183, 209));
    s_female = new QBarSet("Femenino");
    s_female->setColor(QColor(237, 0, 140));

    *s_male <<0<<0<<0<<0;
    *s_female <<0<<0<<0<<0;

    chart = new QChart();

    chart->setTitle("Resultados por sexo y edad");
    chart->setAnimationOptions(QChart::SeriesAnimations);
    series = new QStackedBarSeries(chart);

    series->append(s_male);
    series->append(s_female);

    chart->addSeries(series);
    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    chart->setAxisX(axisX, series);
    axisY = new QValueAxis();

    ui->ageCV->setChart(chart);

    init();
}

ResultsForm::~ResultsForm()
{
    delete ui;
}

void ResultsForm::updateInfo()
{
    process(ui->optionCB->currentIndex() , ui->fromDE->date(), ui->toDE->date());

}

void ResultsForm::init()
{
    ui->optionCB->addItems(titles_option);

    ui->fromDE->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));
    ui->fromDE->setDate(QDate::currentDate());

    ui->toDE->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));
    ui->toDE->setDate(QDate::currentDate());

}

void ResultsForm::calculatePsychological(const QDate &from, const QDate &to)
{

    DQList<Psychological> psychologicals;
    DQList<Assistance_Psychological> assistence_psychologicals;
    DQList<Assistance_Psychological> tmp_psychologicals;

    psychologicals= Psychological::objects().filter(DQWhere("date", ">=", from)&& DQWhere("date", "<=", to)).all();

    total = psychologicals.size();
    for(int i=0; i<total; i++){
        tmp_psychologicals = Assistance_Psychological::objects().filter(DQWhere("psychological")==psychologicals.at(i)->id).all();
        tmp_rows = tmp_psychologicals.size();
        for(int row= 0; row < tmp_rows; row++)
            assistence_psychologicals<<*tmp_psychologicals.at(row);
    }

    total = assistence_psychologicals.size();
    for(int i=0; i < total; i++)
    {
        sex = assistence_psychologicals.at(i)->user->sex->toString();
        id = assistence_psychologicals.at(i)->user->id->toInt();
        age = assistence_psychologicals.at(i)->user->age->toInt();

        calculate(id, age, sex);
    }
}

void ResultsForm::calculateLeadership(const QDate &from, const QDate &to)
{
    DQList<Leadership> leaderships;
    DQList<Assistance_Leadership> assistence_leaderships;
    DQList<Assistance_Leadership> tmp_leaderships;


    leaderships = Leadership::objects().filter(DQWhere("date", ">=", from)&& DQWhere("date", "<=", to)).all();

    total = leaderships.size();
    for(int i=0; i<total; i++){
        tmp_leaderships = Assistance_Leadership::objects().filter(DQWhere("leadership")==leaderships.at(i)->id).all();
        tmp_rows = tmp_leaderships.size();
        for(int row= 0; row < tmp_rows; row++)
            assistence_leaderships<<*tmp_leaderships.at(row);
    }

    total = assistence_leaderships.size();
    for(int i=0; i < total; i++)
    {
        sex = assistence_leaderships.at(i)->user->sex->toString();
        id = assistence_leaderships.at(i)->user->id->toInt();
        age = assistence_leaderships.at(i)->user->age->toInt();
        calculate(id, age, sex);
    }

}

void ResultsForm::calculateFreetime(const QDate &from, const QDate &to)
{
    DQList<Freetime> freetimes;
    DQList<Assistance_Freetime> assistence_freetimes;
    DQList<Assistance_Freetime> tmp_freetimes;

    freetimes= Freetime::objects().filter(DQWhere("date", ">=", from)&& DQWhere("date", "<=", to)).all();

    total = freetimes.size();
    for(int i=0; i<total; i++){
        tmp_freetimes = Assistance_Freetime::objects().filter(DQWhere("freetime")==freetimes.at(i)->id).all();
        tmp_rows = tmp_freetimes.size();
        for(int row= 0; row < tmp_rows; row++)
            assistence_freetimes<<*tmp_freetimes.at(row);
    }

    total = assistence_freetimes.size();

    for(int i=0; i < total; i++)
    {
        sex = assistence_freetimes.at(i)->user->sex->toString();
        id = assistence_freetimes.at(i)->user->id->toInt();
        age = assistence_freetimes.at(i)->user->age->toInt();
        calculate(id, age, sex);
    }
}

void ResultsForm::process(int index, const QDate &from, const QDate &to)
{

    male_list.clear();
    female_list.clear();

    children_male_list.clear();
    children_female_list.clear();
    adolescent_male_list.clear();
    adolescent_female_list.clear();
    young_male_list.clear();
    young_female_list.clear();
    adult_male_list.clear();
    adult_female_list.clear();


    switch (index)
    {
    case all_index:
        calculatePsychological(from, to);
        calculateLeadership(from, to);
        calculateFreetime(from, to);
        break;
    case psychological_index:
        calculatePsychological(from, to);
        break;
    case leadership_index:
        calculateLeadership(from, to);
        break;
    case fretime_index:
        calculateFreetime(from, to);
        break;
    default:
        break;
    }

    /*set values*/

    s_male->replace(children, children_male_list.length());
    s_female->replace(children,children_female_list.length());

    s_male->replace(adolescent, adolescent_male_list.length());
    s_female->replace(adolescent,adolescent_female_list.length());

    s_male->replace(young, young_male_list.length());
    s_female->replace(young,young_female_list.length());

    s_male->replace(adult, adult_male_list.length());
    s_female->replace(adult,adult_female_list.length());

    axisY->setLabelFormat("%d");
    chart->setAxisY(axisY, series);

    chart->removeSeries(series);
    chart->addSeries(series);

}

int ResultsForm::getCategory(const int& age)
{
    if(age>=6 && age<=11)
        return children;
    if(age>=12 && age<=18)
        return adolescent;
    if(age>=19 && age<=25)
        return young;
    return adult;
}

void ResultsForm::calculate(int id, int age, QString sex)
{

    switch (getCategory(age)) {
    case children:
        if(sex == "Masculino"){
            if(!children_male_list.contains(id))
            {
                children_male_list.append(id);
            }
        }
        else{
            if(!children_female_list.contains(id))
            {
                children_female_list.append(id);
            }
        }
        break;
    case adolescent:
        if(sex == "Masculino"){
            if(!adolescent_male_list.contains(id))
            {
                adolescent_male_list.append(id);
            }
        }
        else{
            if(!adolescent_female_list.contains(id))
            {
                adolescent_female_list.append(id);
            }
        }
        break;
    case young:
        if(sex == "Masculino"){
            if(!young_male_list.contains(id))
            {
                young_male_list.append(id);
            }
        }else{
            if(!young_female_list.contains(id))
            {
                young_female_list.append(id);
            }
        }
        break;
    case adult:
        if(sex == "Masculino"){
            if(!adult_male_list.contains(id))
            {
                adult_male_list.append(id);
            }
        }
        else{
            if(!adult_female_list.contains(id))
            {
                adult_female_list.append(id);
            }
        }
        break;
    default:
        break;
    }
}





void ResultsForm::on_optionCB_currentIndexChanged(int index)
{
    process(index, ui->fromDE->date(), ui->toDE->date());
}


void ResultsForm::on_fromDE_dateChanged(const QDate &date)
{
    process(ui->optionCB->currentIndex(), date, ui->toDE->date());
}

void ResultsForm::on_toDE_dateChanged(const QDate &date)
{
    process(ui->optionCB->currentIndex(), ui->fromDE->date(), date);
}


