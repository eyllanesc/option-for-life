#include "leadershipform.h"
#include "ui_leadershipform.h"

#include <QLocale>
#include <QCalendarWidget>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QDateEdit>

#include "Utils.h"

LeadershipForm::LeadershipForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LeadershipForm)
{
    ui->setupUi(this);
    init();
}

LeadershipForm::~LeadershipForm()
{
    delete ui;
}

void LeadershipForm::updateInfo()
{
    filterByUser();
}

void LeadershipForm::init()
{
    ui->localCB->addItems(locals);

    ui->dateEdit->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));

    ui->toDE->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));

    titles_leadership << "Usuario" << "Tema" <<"Local";

    ui->dateEdit->setDate(QDate::currentDate());

    ui->dateEdit_2->calendarWidget()->setLocale(QLocale(QLocale::Spanish, QLocale::Peru));
    ui->dateEdit_2->setDate(QDate::currentDate());

    ui->toDE->setDate(QDate::currentDate());
    filterByUser();
}


bool LeadershipForm::isValid()
{
    if(ui->themeLE->text() == "")
        return false;
    leadership = Leadership();
    leadership.date = ui->dateEdit->date().toString("yyyy-MM-dd");
    leadership.theme = ui->themeLE->text();
    leadership.local = ui->localCB->currentText();
    return true;
}

void LeadershipForm::setId(int id)
{
    mId = id;
}
void LeadershipForm::on_savePB_clicked()
{
    if(Leadership::objects()
            .filter(DQWhere("date", "=", ui->dateEdit->date().toString("yyyy-MM-dd"))&&
                    DQWhere("local", "=", ui->localCB->currentText())&&
                    DQWhere("theme", "=", ui->themeLE->text()))
            .count() == 0)
    {
        if(isValid())
        {
            leadership.save();
            qDebug()<<"Creando un leadership";
        }
        else
        {
            QMessageBox::critical(this, "Liderazgo", "Existen campos vacios");
            return;
        }
    }
    else{
        leadership.load(DQWhere("date", "=", ui->dateEdit->date().toString("yyyy-MM-dd"))&&
                        DQWhere("local" , "=", ui->localCB->currentText())&&
                        DQWhere("theme" , "=", ui->themeLE->text()));
        qDebug()<<leadership.id;
    }

    int nUsers = mUsers.size();
    if((nUsers == 0) || (ui->numberLE->text()=="")){
        QMessageBox::critical(this, "Usuarios", "Seleccione al menos un usuario");
        return;
    }


    for(int i=0; i < nUsers; i++)
    {
        if(Assistance_Leadership::objects()
                .filter(DQWhere("leadership", "=", leadership.id)&&
                        DQWhere("user", "=", mUsers.at(i)->id))
                .count()==0)
        {
            Assistance_Leadership assistance;
            assistance.user = mUsers.at(i)->id;
            assistance.leadership = leadership.id;
            assistance.auth = mId;
            QMessageBox::information(this, "Usuarios","Se guardo la asistencia de "+ mUsers.at(i)->name);
            assistance.save();
        }
    }

    filterByDate(ui->dateEdit_2->date(), ui->dateEdit->date());
}

void LeadershipForm::filterByDate(const QDate &from, const QDate &to)
{

    DQList<Leadership> leaderships = Leadership::objects().filter(DQWhere("date",">=", from.toString("yyyy-MM-dd"))
                                                                  && DQWhere("date","<=", to.toString("yyyy-MM-dd"))).all();

    int length = leaderships.size();

    DQList<Assistance_Leadership> assistances;

    DQList<Assistance_Leadership> tmp;

    int tmp_rows;
    for(int i=0; i < length; i++){
        tmp = Assistance_Leadership::objects().filter(DQWhere("leadership")==leaderships.at(i)->id).all();
        tmp_rows = tmp.size();
        for(int row= 0; row < tmp_rows; row++){
            assistances<<*tmp.at(row);
        }
    }

    int rows = assistances.size();
    int cols = titles_leadership.length();

    QStandardItemModel *model = new QStandardItemModel(rows,cols);

    for(int col = 0; col < cols; col++){
        model->setHorizontalHeaderItem(col,new QStandardItem(titles_leadership.at(col)));
    }

    for(int row = 0; row < rows; row++)
    {
        model->setItem(row,0, new QStandardItem(assistances.at(row)->user->name));
        model->setItem(row,1, new QStandardItem(assistances.at(row)->leadership->theme));
        model->setItem(row,2, new QStandardItem(assistances.at(row)->leadership->local));
    }
    ui->listTV->setModel(model);
}


void LeadershipForm::on_toDE_dateChanged(const QDate &date)
{
    filterByDate(ui->dateEdit_2->date(), date);
}

void LeadershipForm::filterByUser(const QString &q, int index)
{
    if(q=="")
        mUsers = User::objects().all();
    else
        mUsers = User::objects().filter(DQWhere("expedient_number")== q+" "+codes.at(index)).all();

    int rows = mUsers.size();
    int cols = titles.length();

    QStandardItemModel *model = new QStandardItemModel(rows,cols);

    for(int col = 0; col < cols; col++)
        model->setHorizontalHeaderItem(col,new QStandardItem(titles.at(col)));

    for(int row = 0; row < rows; row++)
    {
        model->setItem(row,0, new QStandardItem(mUsers.at(row)->name));
        model->setItem(row,1, new QStandardItem(mUsers.at(row)->sex));
        model->setItem(row,2, new QStandardItem(QString::number(mUsers.at(row)->age->toInt())));

        model->setItem(row,3, new QStandardItem(mUsers.at(row)->life_project->toBool()));
        model->setItem(row,4, new QStandardItem(mUsers.at(row)->vocational_orientation->toBool()));

        model->setItem(row,5, new QStandardItem(mUsers.at(row)->place_of_origin));
        model->setItem(row,6, new QStandardItem(mUsers.at(row)->expedient_number));
        model->setItem(row,7, new QStandardItem(mUsers.at(row)->sector));
    }
    ui->filterTV->setModel(model);
}


void LeadershipForm::on_numberLE_textChanged(const QString &number)
{
    filterByUser(number, ui->localCB->currentIndex());
}

void LeadershipForm::on_localCB_currentIndexChanged(int index)
{
    filterByUser(ui->numberLE->text(), index);
}


void LeadershipForm::on_dateEdit_2_dateChanged(const QDate &date)
{
    filterByDate(date, ui->dateEdit->date());
}
